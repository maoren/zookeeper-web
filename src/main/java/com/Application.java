package com;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.embedded.ConfigurableEmbeddedServletContainer;
import org.springframework.boot.context.embedded.EmbeddedServletContainerCustomizer;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.web.servlet.view.freemarker.FreeMarkerViewResolver;

import java.util.Arrays;
import java.util.Properties;

//@Configuration
//@ComponentScan
//@EnableAutoConfiguration

@EnableAutoConfiguration(exclude = {FreeMarkerViewResolver.class})
@SpringBootApplication
@ImportResource(value = { "spring/zk-springboot-mvc.xml" })
public class Application implements EmbeddedServletContainerCustomizer {
	private static int port=8081;
	private static String contextPath="/zkweb";

	public static void main(String[] args) {
		setCounstomerProp(args);
		ApplicationContext ctx=SpringApplication.run(Application.class, args);
		System.out.println("Let's inspect the beans provided by Spring Boot:");
		String[] beanNames = ctx.getBeanDefinitionNames();
		Arrays.sort(beanNames);
		for (String beanName : beanNames) {
			System.out.println(beanName);
		}
	}

	private static void setCounstomerProp(String[] args){
		System.out.print("java -jar build/libs/zookeeper-web-1.0.0.RELEASE.war -p 8081 -c /zkweb -con_file /tmp/conectionStrings.properties");
		if (args!=null && args.length>=2){
			for (int i = 0; i <args.length ; i++) {
				if(args[i].equals("-p") && i+1<args.length){
					try {
						port=Integer.parseInt(args[i+1]);
					}catch (NumberFormatException e){
					}
				}

				if(args[i].equals("-c") && i+1<args.length){
					contextPath=args[i+1];
				}

				if(args[i].equals("-con_file") && i+1<args.length){
					System.setProperty("con_file", args[i+1]);
				}
			}
		}
	}

	@Override  
    public void customize(ConfigurableEmbeddedServletContainer container) {  
        container.setPort(port);
		container.setContextPath(contextPath);
    }  
}