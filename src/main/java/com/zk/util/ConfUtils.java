package com.zk.util;

import java.io.IOException;
import java.util.Properties;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.support.PropertiesLoaderUtils;

public class ConfUtils {
   private static final Logger LOGGER = LoggerFactory.getLogger(ConfUtils.class);
   private static final String CONF_PATH = "conf/conectionStrings.properties";

   public static Properties getConxtions() {
      Properties properties = null;
      try {
         properties = PropertiesLoaderUtils.loadProperties(new ClassPathResource(CONF_PATH));
         //自定义配置文件，增加灵活性
         String path=System.getProperty("con_file");
         if (StringUtils.isNoneBlank(path)) {
            try {
               Properties properties1 = null;
               properties1 = PropertiesLoaderUtils.loadProperties(new FileSystemResource(path));
               properties.putAll(properties1);
            } catch (Exception e) {
               System.err.print(path + "无效");
            }
         }
      } catch (IOException e) {
         LOGGER.error(e.getMessage(), e);
      }

      return properties;
   }
}
